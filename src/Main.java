

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLOutput;
import java.util.*;

class Bowler{
    String bowlerName;
    double economic;
    Bowler(String bowlerName,double economic){
        this.bowlerName = bowlerName;
        this.economic = economic;
    }
}
class RunWicket{
    int run,wicket;
    RunWicket(int run,int wicket){
        this.run = run;
        this.wicket = wicket;
    }
}
public class Main {

    public HashMap<String,Integer> numberOfMatchesPlayedPerYear(List<Matches> matches){
        HashMap<String,Integer> matchesPerYear  = new HashMap<>();
        for(Matches match:matches){
            if(match.getSeason().equals("season")){
                continue;
            }
            matchesPerYear.put(match.getSeason(),matchesPerYear.getOrDefault(match.getSeason(),0)+1);
        }
        System.out.println("\nMatches Played Per Season:\n");
        for (Map.Entry<String, Integer> season : matchesPerYear.entrySet()){
            System.out.println("Season: " + season.getKey() + " Matches: " + season.getValue());
        }
        return matchesPerYear;
    }
    public HashMap<String ,Integer> matchesWonPerTeam(List<Matches> matches){
        HashMap<String ,Integer> matchesWonPerTeams = new HashMap<>();
        for(Matches data: matches){
            if(data.getId().equals("id")){
                continue;
            }
            String winner = data.getWinner();
            //System.out.println(winner);
            if(!winner.isEmpty()) {
                matchesWonPerTeams.put(winner, matchesWonPerTeams.getOrDefault(winner, 0) + 1);
            }
        }
//        System.out.println("\nMatches Won Per Team:\n");
//        for(Map.Entry<String,Integer> team : matchesWonPerTeams.entrySet()){
//            System.out.println("Team: " + team.getKey() + "\nNo of Matches Won: " + team.getValue()+"\n");
//        }
        System.out.println(matchesWonPerTeams);
        return matchesWonPerTeams;
    }
    public HashMap<String ,Integer> extraRunsConcededPerTeam(List<Matches> matches,List<Deliveries> deliveries){
        HashMap<String ,Integer> extraRunsConcededPerTeam = new HashMap<>();
        List<String> matchId = new ArrayList<>();
        for(Matches data:matches){
            if(data.getSeason().equals("2016")){
                matchId.add(data.getId());
            }
        }
        for(Deliveries data:deliveries){
            if(matchId.contains(data.getMatch_id())){
                String team = data.getBowling_team();
                int extraRun = Integer.parseInt(data.getExtra_runs());
                extraRunsConcededPerTeam.put(team,extraRunsConcededPerTeam.getOrDefault(team,0)+extraRun);
            }
        }
//        System.out.println("Extra Runs conceded in the Year 2016:\n");
//        for(Map.Entry<String,Integer> team : extraRunsConcededPerTeam.entrySet()){
//            System.out.println("Team: " + team.getKey() + "\nExtra Runs Conceded: "+team.getValue()+"\n");
//        }
        System.out.println(extraRunsConcededPerTeam);
        return extraRunsConcededPerTeam;
    }
    public Map<String,Double> topEconomicalBowlers(List<Matches> matches,List<Deliveries> deliveries){
        HashMap<String,Integer> totalRun = new HashMap<>();
        HashMap<String,Integer> totalBall = new HashMap<>();
        ArrayList<Bowler> economicalBowlers = new ArrayList<>();
        List<String>matchId = new ArrayList<>();
        for(Matches data:matches){
            if(data.getSeason().equals("2015")){
                matchId.add(data.getId());
            }
        }
        for(Deliveries data:deliveries){
            if(matchId.contains(data.getMatch_id())){
                int totalrun = 0;
                int totalball = 0;
                if(data.getWide_runs().equals("0")&&data.getNoball_runs().equals("0")){
                    totalball+=1;
                }
                if(data.getBye_runs().equals("0")&&data.getLeg_by_run().equals("0")){
                    totalrun+=Integer.parseInt(data.getTotal_runs());
                }
                totalRun.put(data.getBowler(),totalRun.getOrDefault(data.getBowler(),0)+totalrun);
                totalBall.put(data.getBowler(),totalBall.getOrDefault(data.getBowler(),0)+totalball);
            }
        }
        for(String data:totalRun.keySet()){
            if(totalBall.containsKey(data)){
                double over = totalBall.get(data)/(double)6;
                double economic = totalRun.get(data)/over;
                economicalBowlers.add(new Bowler(data,economic));
            }
        }
        Collections.sort(economicalBowlers, new Comparator<Bowler>() {
            @Override
            public int compare(Bowler o1, Bowler o2) {
                if(o1.economic-o2.economic == 0) return 0;
                else if(o1.economic > o2.economic) return 1;
                else return -1;
            }
        });
        System.out.println("Top 10 Economical Bowlers:\n");
        List<Bowler> topEconomicalBowlers=economicalBowlers.subList(0,10);
        Map<String,Double> topEco=new LinkedHashMap<>();
        for(Bowler data:topEconomicalBowlers){
            topEco.put(data.bowlerName,data.economic);
        }
       System.out.println(topEco);
        return topEco;
    }
    public HashMap <String,Integer> highestDismissal(List<Deliveries> deliveries){
        HashMap <String,Integer> result=new HashMap<>();
        for(Deliveries delivery:deliveries){
            if(delivery.getPlayer_dismissed()!=null){
                result.put(delivery.getPlayer_dismissed()+" dismissed by "+delivery.getBowler(),result.getOrDefault(delivery.getPlayer_dismissed()+" dismissed by "+delivery.getBowler(),0)+1);
            }
        }
        HashMap <String,Integer> dismiss=maxDismiss(result);
        System.out.println("Highest no of times a player is dismissed by another player:\n");
        for(Map.Entry<String,Integer> output : dismiss.entrySet()){
            System.out.println(output.getKey()+":"+output.getValue());
        }
        return dismiss;
    }
    public HashMap <String,Integer> maxDismiss(HashMap<String,Integer> result){
        int max=Integer.MIN_VALUE;
        HashMap<String,Integer> dismiss=new HashMap<>();
        for(Map.Entry<String,Integer> dismissal: result.entrySet()){
            String pair=dismissal.getKey();
            int dismissed=dismissal.getValue();
            if(dismissed>max){
                dismiss.clear();
                max=dismissed;
                dismiss.put(pair,dismissed);
            }
            if(dismissed==max){
                dismiss.put(pair,dismissed);
            }
        }
        return dismiss;
    }

    public static void main(String[] args) {
        Main obj = new Main();
        String deliveriesCSV = "/Users/antarikshabarua/IdeaProjects/Antariksha_IPL_JAVA/src/deliveries.csv";
        String matchesCSV = "/Users/antarikshabarua/IdeaProjects/Antariksha_IPL_JAVA/src/matches.csv";

        List<Matches> matches= getMatchesData(matchesCSV);
        List<Deliveries> deliveries = getDeliveriesData(deliveriesCSV);

        obj.numberOfMatchesPlayedPerYear(matches);

        obj.matchesWonPerTeam(matches);

        obj.extraRunsConcededPerTeam(matches,deliveries);

        obj.topEconomicalBowlers(matches,deliveries);

        obj.highestDismissal(deliveries);

    }

    public static List<Deliveries> getDeliveriesData(String pathDeliveries) {
        List<Deliveries> deliveries = new ArrayList<>();
        String line = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(pathDeliveries));
            while((line = reader.readLine()) != null) {
                String data[] = line.split(",");
                Deliveries delivery= new Deliveries();
                delivery.setMatch_id(data[0]);
                delivery.setInning(data[1]);
                delivery.setBatting_team(data[2]);
                delivery.setBowling_team(data[3]);
                delivery.setOver(data[4]);
                delivery.setBall(data[5]);
                delivery.setBatsman(data[6]);
                delivery.setNon_striker(data[7]);
                delivery.setBowler(data[8]);
                delivery.setIs_supper_over(data[9]);
                delivery.setWide_runs(data[10]);
                delivery.setBye_runs(data[11]);
                delivery.setLeg_by_run(data[12]);
                delivery.setNoball_runs(data[13]);
                delivery.setPenalty_runs(data[14]);
                delivery.setBatsman_run(data[15]);
                delivery.setExtra_runs(data[16]);
                delivery.setTotal_runs(data[17]);
                if(data.length>=19)
                    delivery.setPlayer_dismissed(data[18]);
                if(data.length>=20)
                    delivery.setDismissed_kind(data[19]);
                if(data.length>=21)
                    delivery.setFielder(data[20]);
                deliveries.add(delivery);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return deliveries;
    }
    public static List<Matches> getMatchesData(String pathMatches) {
        List<Matches> matches= new ArrayList<>();
        String line = "";
        try {
            BufferedReader reader = new BufferedReader(new FileReader(pathMatches));
            while((line =reader.readLine()) != null) {
                String data[] = line.split(",");
                Matches match = new Matches();
                match.setId(data[0]);
                match.setSeason(data[1]);
                match.setCity(data[2]);
                match.setDate(data[3]);
                match.setTeam1(data[4]);
                match.setTeam2(data[5]);
                match.setToss_winner(data[6]);
                match.setToss_decision(data[7]);
                match.setResult(data[8]);
                match.setDl_applied(data[9]);
                match.setWinner(data[10]);
                match.setWin_by_run(data[11]);
                match.setWi_by_wicket(data[12]);
                match.setPlayer_of_match(data[13]);
                match.setVenue(data[14]);
                if(data.length>=16) {
                    match.setUmpire1(data[15]);
                }
                if(data.length>=17) {
                    match.setUmpire2(data[16]);
                }
                if(data.length==18) {
                    match.setUmpire3(data[17]);
                }
                matches.add(match);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return matches;
    }
}
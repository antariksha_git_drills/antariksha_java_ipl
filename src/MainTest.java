
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MainTest {
    // Sample data for Matches class
    @Test
    void numberOfMatchesPlayedPerYearTest(){
        var test=new Main();
        List<Matches> matches = new ArrayList<>();

        Matches match1 = new Matches();
        match1.setId("1");
        match1.setSeason("2019");
        //System.out.println(match1.getSeason());

        Matches match2 = new Matches();
        match2.setId("2");
        match2.setSeason("2019");
        //System.out.println(match2.getSeason());

        Matches match3 = new Matches();
        match3.setId("3");
        match3.setSeason("2018");
        //System.out.println(match3.getSeason());

        Matches match4 = new Matches();
        match4.setId("4");
        match4.setSeason("2018");
        //System.out.println(match4.getSeason());

        Matches match5 = new Matches();
        match5.setId("5");
        match5.setSeason("2018");
        //System.out.println(match5.getSeason());
// set values for match2...

        matches.add(match1);
        matches.add(match2);
        matches.add(match3);
        matches.add(match4);
        matches.add(match5);

        HashMap<String,Integer> sampleOutput=new HashMap<>();

        sampleOutput.put("2019",2);
        sampleOutput.put("2018",3);

        assertEquals(sampleOutput,test.numberOfMatchesPlayedPerYear(matches));
    }
    @Test
    void matchesWonPerTeamTest(){
        var Test=new Main();
        List <Matches> sampleInput=new ArrayList<>();

        Matches m1=new Matches();
        m1.setId("1");
        m1.setWinner("A");
        //System.out.println(m1.getWinner());

        Matches m2=new Matches();
        m2.setId("2");
        m2.setWinner("A");
        //System.out.println(m2.getWinner());

        Matches m3=new Matches();
        m3.setId("3");
        m3.setWinner("B");
        //System.out.println(m3.getWinner());

        Matches m4=new Matches();
        m4.setId("4");
        m4.setWinner("C");

        Matches m5=new Matches();
        m5.setId("5");
        m5.setWinner("C");

        Matches m6=new Matches();
        m6.setId("6");
        m6.setWinner("C");

        Matches m7=new Matches();
        m7.setId("7");
        m7.setWinner("C");

        Matches m8=new Matches();
        m8.setId("8");
        m8.setWinner("D");

        Matches m9=new Matches();
        m9.setId("9");
        m9.setWinner("D");

        Matches m10=new Matches();
        m10.setId("10");
        m10.setWinner("E");

        sampleInput.add(m1);
        sampleInput.add(m2);
        sampleInput.add(m3);
        sampleInput.add(m4);
        sampleInput.add(m5);
        sampleInput.add(m6);
        sampleInput.add(m7);
        sampleInput.add(m8);
        sampleInput.add(m9);
        sampleInput.add(m10);

        HashMap<String,Integer> sampleOutput=new HashMap<>();

        sampleOutput.put("A",2);
        sampleOutput.put("B",1);
        sampleOutput.put("C",4);
        sampleOutput.put("D",2);
        sampleOutput.put("E",1);

        assertEquals(sampleOutput,Test.matchesWonPerTeam(sampleInput));
    }
    @Test
    void extraRunsConcedePerTeamTest(){
        var Test=new Main();
        List <Matches> sampleInputMatch=new ArrayList<>();

        Matches m1=new Matches();
        m1.setId("1");
        m1.setSeason("2014");

        Matches m2=new Matches();
        m2.setId("2");
        m2.setSeason("2016");

        Matches m3=new Matches();
        m3.setId("3");
        m3.setSeason("2016");

        sampleInputMatch.add(m1);
        sampleInputMatch.add(m2);
        sampleInputMatch.add(m3);

        List <Deliveries> sampleInputDelivery=new ArrayList<>();

        Deliveries d1=new Deliveries();
        d1.setMatch_id("1");
        d1.setBowling_team("A");
        d1.setExtra_runs("1");

        Deliveries d2=new Deliveries();
        d2.setMatch_id("1");
        d2.setBowling_team("B");
        d2.setExtra_runs("2");

        Deliveries d3=new Deliveries();
        d3.setMatch_id("2");
        d3.setBowling_team("C");
        d3.setExtra_runs("3");

        Deliveries d4=new Deliveries();
        d4.setMatch_id("2");
        d4.setBowling_team("C");
        d4.setExtra_runs("4");

        Deliveries d5=new Deliveries();
        d5.setMatch_id("2");
        d5.setBowling_team("D");
        d5.setExtra_runs("5");

        Deliveries d6=new Deliveries();
        d6.setMatch_id("2");
        d6.setBowling_team("D");
        d6.setExtra_runs("6");

        Deliveries d7=new Deliveries();
        d7.setMatch_id("3");
        d7.setBowling_team("B");
        d7.setExtra_runs("5");

        Deliveries d8=new Deliveries();
        d8.setMatch_id("3");
        d8.setBowling_team("A");
        d8.setExtra_runs("4");

        Deliveries d9=new Deliveries();
        d9.setMatch_id("3");
        d9.setBowling_team("B");
        d9.setExtra_runs("3");

        Deliveries d10=new Deliveries();
        d10.setMatch_id("3");
        d10.setBowling_team("A");
        d10.setExtra_runs("2");

        sampleInputDelivery.add(d1);
        sampleInputDelivery.add(d2);
        sampleInputDelivery.add(d3);
        sampleInputDelivery.add(d4);
        sampleInputDelivery.add(d5);
        sampleInputDelivery.add(d6);
        sampleInputDelivery.add(d7);
        sampleInputDelivery.add(d8);
        sampleInputDelivery.add(d9);
        sampleInputDelivery.add(d10);

        HashMap<String,Integer> sampleOutput=new HashMap<>();

        sampleOutput.put("C",7);
        sampleOutput.put("D",11);
        sampleOutput.put("B",8);
        sampleOutput.put("A",6);

        assertEquals(sampleOutput,Test.extraRunsConcededPerTeam(sampleInputMatch,sampleInputDelivery));
    }
    @Test
    void topEconomicBowlersTest(){

        var Test=new Main();

        List<Matches> sampleInputMatch=new ArrayList<>();

        Matches m1=new Matches();
        m1.setId("1");
        m1.setSeason("2015");

        Matches m2=new Matches();
        m2.setId("2");
        m2.setSeason("2016");

        Matches m3=new Matches();
        m3.setId("3");
        m3.setSeason("2015");

        sampleInputMatch.add(m1);
        sampleInputMatch.add(m2);
        sampleInputMatch.add(m3);

        List<Deliveries> sampleInputDelivery=new ArrayList<>();

        Deliveries d1=new Deliveries();
        d1.setMatch_id("1");
        d1.setWide_runs("0");
        d1.setNoball_runs("0");
        d1.setBye_runs("0");
        d1.setLeg_by_run("0");
        d1.setBowler("A");
        d1.setTotal_runs("4");

        Deliveries d2=new Deliveries();
        d2.setMatch_id("1");
        d2.setWide_runs("0");
        d2.setNoball_runs("0");
        d2.setBye_runs("1");
        d2.setLeg_by_run("0");
        d2.setBowler("B");
        d2.setTotal_runs("1");

        Deliveries d3=new Deliveries();
        d3.setMatch_id("2");
        d3.setWide_runs("0");
        d3.setNoball_runs("0");
        d3.setBye_runs("0");
        d3.setLeg_by_run("0");
        d3.setBowler("A");
        d3.setTotal_runs("1");

        Deliveries d4=new Deliveries();
        d4.setMatch_id("3");
        d4.setWide_runs("0");
        d4.setNoball_runs("0");
        d4.setBye_runs("0");
        d4.setLeg_by_run("0");
        d4.setBowler("C");
        d4.setTotal_runs("1");

        Deliveries d5=new Deliveries();
        d5.setMatch_id("1");
        d5.setWide_runs("0");
        d5.setNoball_runs("0");
        d5.setBye_runs("0");
        d5.setLeg_by_run("0");
        d5.setBowler("D");
        d5.setTotal_runs("4");

        Deliveries d6=new Deliveries();
        d6.setMatch_id("3");
        d6.setWide_runs("0");
        d6.setNoball_runs("0");
        d6.setBye_runs("0");
        d6.setLeg_by_run("0");
        d6.setBowler("E");
        d6.setTotal_runs("2");

        Deliveries d7=new Deliveries();
        d7.setMatch_id("1");
        d7.setWide_runs("0");
        d7.setNoball_runs("0");
        d7.setBye_runs("0");
        d7.setLeg_by_run("0");
        d7.setBowler("F");
        d7.setTotal_runs("6");

        Deliveries d8=new Deliveries();
        d8.setMatch_id("1");
        d8.setWide_runs("0");
        d8.setNoball_runs("0");
        d8.setBye_runs("0");
        d8.setLeg_by_run("0");
        d8.setBowler("G");
        d8.setTotal_runs("0");

        Deliveries d9=new Deliveries();
        d9.setMatch_id("1");
        d9.setWide_runs("0");
        d9.setNoball_runs("0");
        d9.setBye_runs("0");
        d9.setLeg_by_run("0");
        d9.setBowler("H");
        d9.setTotal_runs("3");

        Deliveries d10=new Deliveries();
        d10.setMatch_id("1");
        d10.setWide_runs("0");
        d10.setNoball_runs("0");
        d10.setBye_runs("0");
        d10.setLeg_by_run("0");
        d10.setBowler("I");
        d10.setTotal_runs("0");

        Deliveries d11=new Deliveries();
        d11.setMatch_id("3");
        d11.setWide_runs("0");
        d11.setNoball_runs("0");
        d11.setBye_runs("0");
        d11.setLeg_by_run("0");
        d11.setBowler("J");
        d11.setTotal_runs("4");

        Deliveries d12=new Deliveries();
        d12.setMatch_id("3");
        d12.setWide_runs("0");
        d12.setNoball_runs("0");
        d12.setBye_runs("0");
        d12.setLeg_by_run("0");
        d12.setBowler("J");
        d12.setTotal_runs("6");

        Deliveries d13=new Deliveries();
        d13.setMatch_id("3");
        d13.setWide_runs("0");
        d13.setNoball_runs("0");
        d13.setBye_runs("0");
        d13.setLeg_by_run("0");
        d13.setBowler("B");
        d13.setTotal_runs("2");

        Deliveries d14=new Deliveries();
        d14.setMatch_id("3");
        d14.setWide_runs("0");
        d14.setNoball_runs("0");
        d14.setBye_runs("0");
        d14.setLeg_by_run("0");
        d14.setBowler("C");
        d14.setTotal_runs("2");

        Deliveries d15=new Deliveries();
        d15.setMatch_id("1");
        d15.setWide_runs("0");
        d15.setNoball_runs("0");
        d15.setBye_runs("0");
        d15.setLeg_by_run("0");
        d15.setBowler("D");
        d15.setTotal_runs("1");

        Deliveries d16=new Deliveries();
        d16.setMatch_id("3");
        d16.setWide_runs("1");
        d16.setNoball_runs("0");
        d16.setBye_runs("0");
        d16.setLeg_by_run("0");
        d16.setBowler("E");
        d16.setTotal_runs("1");

        Deliveries d17=new Deliveries();
        d17.setMatch_id("3");
        d17.setWide_runs("0");
        d17.setNoball_runs("1");
        d17.setBye_runs("0");
        d17.setLeg_by_run("0");
        d17.setBowler("F");
        d17.setTotal_runs("1");

        Deliveries d18=new Deliveries();
        d18.setMatch_id("3");
        d18.setWide_runs("0");
        d18.setNoball_runs("0");
        d18.setBye_runs("1");
        d18.setLeg_by_run("0");
        d18.setBowler("G");
        d18.setTotal_runs("1");

        Deliveries d19=new Deliveries();
        d19.setMatch_id("3");
        d19.setWide_runs("0");
        d19.setNoball_runs("0");
        d19.setBye_runs("0");
        d19.setLeg_by_run("1");
        d19.setBowler("H");
        d19.setTotal_runs("1");

        Deliveries d20=new Deliveries();
        d20.setMatch_id("3");
        d20.setWide_runs("0");
        d20.setNoball_runs("0");
        d20.setBye_runs("0");
        d20.setLeg_by_run("0");
        d20.setBowler("I");
        d20.setTotal_runs("2");

        sampleInputDelivery.add(d1);
        sampleInputDelivery.add(d2);
        sampleInputDelivery.add(d3);
        sampleInputDelivery.add(d4);
        sampleInputDelivery.add(d5);
        sampleInputDelivery.add(d6);
        sampleInputDelivery.add(d7);
        sampleInputDelivery.add(d8);
        sampleInputDelivery.add(d9);
        sampleInputDelivery.add(d10);
        sampleInputDelivery.add(d11);
        sampleInputDelivery.add(d12);
        sampleInputDelivery.add(d13);
        sampleInputDelivery.add(d14);
        sampleInputDelivery.add(d15);
        sampleInputDelivery.add(d16);
        sampleInputDelivery.add(d17);
        sampleInputDelivery.add(d18);
        sampleInputDelivery.add(d19);
        sampleInputDelivery.add(d20);

        Map<String,Double> sampleOutput=new LinkedHashMap<>();

        sampleOutput.put("G",0.0*6);
        sampleOutput.put("B",1.0*6);
        sampleOutput.put("I",1.0*6);
        sampleOutput.put("C",1.5*6);
        sampleOutput.put("H",1.5*6);
        sampleOutput.put("D",2.5*6);
        sampleOutput.put("E",3.0*6);
        sampleOutput.put("A",4.0*6);
        sampleOutput.put("J",5.0*6);
        sampleOutput.put("F",7.0*6);

        assertEquals(sampleOutput,Test.topEconomicalBowlers(sampleInputMatch,sampleInputDelivery));
    }

    @Test
    void maxDismissTest(){
        var Test=new Main();

        List<Deliveries> sampleInput=new ArrayList<>();

        Deliveries d1=new Deliveries();
        d1.setMatch_id("1");
        d1.setPlayer_dismissed("A");
        d1.setBowler("B");

        Deliveries d2=new Deliveries();
        d2.setMatch_id("2");
        d2.setPlayer_dismissed("A");
        d2.setBowler("B");

        Deliveries d3=new Deliveries();
        d3.setMatch_id("3");
        d3.setPlayer_dismissed("A");
        d3.setBowler("B");

        Deliveries d4=new Deliveries();
        d4.setMatch_id("4");
        d4.setPlayer_dismissed("B");
        d4.setBowler("C");

        Deliveries d5=new Deliveries();
        d5.setMatch_id("5");
        d5.setPlayer_dismissed("B");
        d5.setBowler("C");

        Deliveries d6=new Deliveries();
        d6.setMatch_id("6");
        d6.setPlayer_dismissed("B");
        d6.setBowler("C");

        Deliveries d7=new Deliveries();
        d7.setMatch_id("7");
        d7.setPlayer_dismissed("C");
        d7.setBowler("A");

        Deliveries d8=new Deliveries();
        d8.setMatch_id("8");
        d8.setPlayer_dismissed("C");
        d8.setBowler("A");

        Deliveries d9=new Deliveries();
        d9.setMatch_id("9");
        d9.setPlayer_dismissed("C");
        d9.setBowler("A");

        Deliveries d10=new Deliveries();
        d10.setMatch_id("10");
        d10.setPlayer_dismissed("C");
        d10.setBowler("A");

        sampleInput.add(d1);
        sampleInput.add(d2);
        sampleInput.add(d3);
        sampleInput.add(d4);
        sampleInput.add(d5);
        sampleInput.add(d6);
        sampleInput.add(d7);
        sampleInput.add(d8);
        sampleInput.add(d9);
        sampleInput.add(d10);

        HashMap <String,Integer> sampleOutput=new HashMap<>();

        sampleOutput.put("C dismissed by A",4);

        assertEquals(sampleOutput,Test.highestDismissal(sampleInput));
    }
}
